import axios from "axios";
const BASE_URL = "https://api.airtable.com/v0/"
const apiToken = "keyZIIVNiQPvozEWb"

export default axios.create({
  baseURL: BASE_URL,
  headers: {
    "Authorization": "Bearer " + apiToken,
    "Content-Type": "application/json"
  }
});