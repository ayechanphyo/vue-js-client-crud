import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("./components/Home")
    },
    {
      path: "/add-user",
      name: "add",
      component: () => import("./components/NewUser")
    },
    {
      path: "/settings",
      name: "settings",
      component: () => import("./components/Settings")
    },
    {
      path: "/test",
      name: "test",
      component: () => import("./components/TestUsingIndexeddb")
    }
  ]
});